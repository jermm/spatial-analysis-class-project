# server.rb
# what runs the API and website

require 'sinatra'
require 'redcarpet' #markdown
require 'json' #converts ruby structure to json
require 'sequel'
require 'parseconfig'
require './geo_distance.rb'

# set the post and ip
set :port, 80
set :bind, '107.170.234.219'

markdown = Redcarpet::Markdown.new(Redcarpet::Render::HTML)

# open config file
databaseConfigFile = ParseConfig.new('/home/scripts/game/engr-180-project/config/database.conf')

# connect to the database.
adapter = databaseConfigFile['adapter']
host = databaseConfigFile['host']
username = databaseConfigFile['username']
password = databaseConfigFile['password']
database = databaseConfigFile['database']

DB = Sequel.connect(:adapter=> adapter, :host=> host , :username => username, :password => password, :database => database)

# helper functions:
# returns string of file in /public folder
def readFile(file)

	return File.read(File.join('public', "#{file}"))

end

# returns JSON string, sets content type to :json
def convertToJson(output)

	# set encoding options
	encoding_options = {
    :invalid           => :replace,  # Replace invalid byte sequences
    :undef             => :replace,  # Replace anything not defined in ASCII
    :replace           => '',        # Use a blank for those replacements
    :universal_newline => true       # Always break lines with \n
      }
    
    # change content type of the webpage to json.
    content_type :json

	return JSON.pretty_generate(output)

end

# do nothing for favicon.*
get '/favicon.*/?' do

end

#if the get request is /api or /api/
get '/api/?' do

	#create a new hash structure
	output = Hash.new

	#add some elements
	output["online"] = "1"

	#output the convered json.
	convertToJson(output)

end

get '/api/help/?' do

	'TODO: API guide here.'

end

# current gets list of all experiences within 50km (does not sort.)
get '/api/type=near_experiences/lat=:lat/lon=:lon' do

	test = DB[:experiences]

	#create a new hash structure
	output = Hash.new

	#add some elements
	output["online"] = "1"
	output["lat"] = params[:lat]
	output["lon"] = params[:lon]
	output["count"] = test.count

	data = Hash.new

	test.each do |row|

		rowData = Hash.new

		rowData["name"] = row[:exp_name]
		rowData["description"] = row[:exp_description]
		rowData["distance"] = GeoDistance.coorDist(output["lat"].to_f, output["lon"].to_f, row[:start_lat].to_f, row[:start_lon].to_f)

		if (rowData["distance"].to_f < 50)
			data[row[:exp_name]] = rowData
		end

	end

	output["experiences"] = data

	#output the convered json.
	convertToJson(output)

end

get '/api/type=check_in/id=:id/exp_name=:exp_name/step_name=:step_name/lat=:lat/lon=:lon' do

	#create a new hash structure
	output = Hash.new

	time = Time.now
	output["time"] = time

	# convert exp_name to id.
	exName = params[:exp_name]
	stepName = params[:step_name]
	userId = params[:id].to_i
	lat = params[:lat].to_f
	lon = params[:lon].to_f

	exId = nil
	stepId = nil

	experience = DB[:experiences].filter(:exp_name => "#{exName}").first;

	output["title"] = 0
	output["step"] = 0

	# check if title is valid
	if (experience != nil)
		exId = experience[:exp_id]
		output["title"] = 1
	else #title name isn't found in database.
		return convertToJson(output)
	end

	# check if the step name is valid
	step = DB[:steps].filter(:exp_id => "#{exId}", :step_name => "#{stepName}").first;

	if (step != nil)
		stepId = step[:step_id]
		output["step"] = 1
	else
		return convertToJson(output)
	end


	checkIn = DB[:user_spots]

	# hopefully will insert correctly
	checkInStatus = checkIn.insert(:exp_id => exId, :step_id => stepId, :user_id => userId, :lat => lat, :lon => lon, :time => time)

	output["status"] = 0

	if (checkInStatus == 0)
		output["status"] = 1
	end

	return convertToJson(output)

end

# get information about step.
get '/api/type=get_step/exp_name=:exp_name/step_name=:step_name' do

	# convert exp_name to id.
	exName = params[:exp_name]
	stepName = params[:step_name]

	puts stepName

	experience = DB[:experiences].filter(:exp_name => "#{exName}").first;

	#create a new hash structure
	output = Hash.new

	output["title"] = 0
	output["step"] = 0

	exId = nil;

	# check if title is valid
	if (experience != nil)
		exId = experience[:exp_id]
		output["title"] = 1
	else #title name isn't found in database.
		return convertToJson(output)
	end

	# check if the step name is valid
	step = DB[:steps].filter(:exp_id => "#{exId}", :step_name => "#{stepName}").first;

	# if stepname in blank, then get the level 0 information.
	if (stepName == "\"\"")

		step = DB[:steps].filter(:exp_id => "#{exId}".to_i, :required_level => 0).first;
	end

	if (step != nil)

		puts "step found."
		output["step_description"] = markdown.render(readFile("#{step[:step_description]}"))
		output["step_name"] = "#{step[:step_name]}"
		output["current_level"] = "#{step[:required_level]}"
		output["step"] = 1

	else
		return convertToJson(output)
	end

	convertToJson(output)

end



# check if current step has been fullfilled
get '/api/type=check_step/exp_name=:exp_name/level=:level/lat=:lat/lon=:lon' do

	# convert exp_name to id.
	exName = params[:exp_name]
	level = params[:level]
	experience = DB[:experiences].filter(:exp_name => "#{exName}").first;

	#create a new hash structure
	output = Hash.new

	output["title"] = 0
	
	exId = nil;

	# check if title is valid
	if (experience != nil)
		exId = experience[:exp_id]
		output["title"] = 1
	else #title name isn't found in database.
		return convertToJson(output)
	end

	# lookup on step table.
	inputLat = params[:lat].to_f
	inputLon = params[:lon].to_f

	step = DB[:steps].filter(:exp_id => "#{exId}", :required_level => "#{level}");

	step.each do |row|

		distance = GeoDistance.coorDist(row[:lat].to_f, row[:lon].to_f, inputLat, inputLon)
		output["distance"] = distance
		output["new_step"] = 0

		# TODO: move to config file.
		if (distance < 0.15)

			# new step
			output["new_level"] = row[:output_level]

			# check database for next step:
			nextStep = DB[:steps].filter(:exp_id => "#{exId}", :required_level => "#{row[:output_level]}").first

			if (nextStep != nil)

				output["step_description"] = markdown.render(readFile(nextStep[:step_description]))
				output["step_name"] = nextStep[:step_name]
				output["current_level"] = "#{row[:output_level]}"
				output["new_step"] = 1

			end

		end
		
	end

	convertToJson(output)

end

get '/api/type=list_all_exps' do

	#create a new hash structure
	output = Hash.new

	experiences = DB[:experiences]

	experiences.each do |row|

		line = Hash.new

		line["exp_name"] = row[:exp_name]
		line["exp_description"] = row[:exp_description]
		output[row[:exp_id]] = line

	end

	return convertToJson(output)

end


get '/api/type=list_all_steps/:exp_name' do 

	
	exName = params[:exp_name]
	output = Hash.new

	experience = DB[:experiences].filter(:exp_name => "#{exName}").first;	

	if (experience != nil)
		exId = experience[:exp_id]

	else #title name isn't found in database.
		output["title"] = 0
		return convertToJson(output)
	end

	steps = DB[:steps].filter(:exp_id => exId);

	steps.each do |row|
		
		line = Hash.new

		line["step_name"] = row[:step_name]
		line["lat"] = row[:lat];
		line["lon"] = row[:lon];
		line["checkin_count"] = DB[:user_spots].filter(:exp_id => exId, :step_id => row[:step_id]).count
		output[row[:step_id]] = line

	end

	return convertToJson(output)

end

# return user checkins.
get '/api/type=user_stats/exp_name=:exp_name/step_name=:step_name' do

	exName = params[:exp_name]
	experience = DB[:experiences].filter(:exp_name => "#{exName}").first;

	#create a new hash structure
	output = Hash.new

	exId = nil;

	# check if title is valid
	if (experience != nil)
		exId = experience[:exp_id]

	else #title name isn't found in database.
		output["title"] = 0
		return convertToJson(output)
	end


	step = DB[:steps].filter(:step_name => params[:step_name], :exp_id => exId).first;

	stepLat = step[:lat]
	stepLon = step[:lon]

	if (step != nil)
	
		stepID = step[:step_id]
		userSpots = DB[:user_spots].filter(:step_id => stepID)
		spotCounter = 0;

		userSpots.each do |row|

			line = Hash.new

			line["user_id"] = row[:user_id]
			line["lat"] = row[:lat]
			line["lon"] = row[:lon]
			line["time"] = row[:time]
			line["distance"] = GeoDistance.coorDist(row[:lat], row[:lon], stepLat, stepLon)
			
			output["#{spotCounter}"] = line;
			spotCounter += 1

		end

		output["actual_lat"]  = step[:lat]
		output["actual_lon"] = step[:lon]

	else

		output["step"] = 0
		return convertToJson(output)
	end

	convertToJson(output)

end

# everything else is a bad API call.
get '/api/*' do

	#create a new hash structure
	output = Hash.new

	output["valid"] = false

	return convertToJson(output)

end

# load the info page
['/info/?', "/info/:value/?","/info/:value/:value2/?"].each do |info_page|

	get info_page do

		output = "<!DOCTYPE html>"

		output += readFile("css.txt")
		output += readFile("info_page.html")
		output + readFile("info_page_loads.txt")

	end

end

# load test page.
get '/testing_old/?' do

	# set doctype
	output = "<!DOCTYPE html><html><head>"

	# read in css
	output += readFile("css.txt")
	output += "</head><body>"

	#read in and render testing.md
	output += markdown.render(readFile("testing_old.md"))

	#load js
	output += readFile("locationTest.js")

	#get users location and output the page.
	output + "<script>getLocation();</script></html>"

end

# load homepage
["/?", "/index*"].each do |home|
	get home do

		# "in-progress"
		output = "<!DOCTYPE html>"

		# read in css
		output += readFile("css.txt")
		output += readFile("home.html")
		output += "\n<script src=\"//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js\"></script>"
		output += "\n<script src=\"//locusgame.net/generateList.js\"></script>"

	end
end

# load game.js
get '/:exp_name/:step_name/?' do

	output = "<!DOCTYPE html>"

	# read in css
	output += readFile("css.txt")
	output += readFile("game.html")
	output += "\n<script src=\"//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js\"></script>"
	output + "\n<script src=\"//locusgame.net/game.js\"></script>"

end

# load game.js
get '/:exp_name/?' do

	output = "<!DOCTYPE html>"

	# read in css
	output += readFile("css.txt")
	output += readFile("game.html")
	output += "\n<script src=\"//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js\"></script>"
	output + "\n<script src=\"//locusgame.net/game.js\"></script>"
	
end

# return robots.txt
get '/robots.txt' do

	readFile('robots.txt')

end

# anything else show this
get '/*' do

	output = "<!DOCTYPE html>"
	output += readFile("css.txt")
	output + "in-progress"

end
