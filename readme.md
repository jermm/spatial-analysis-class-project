#Untiled ENGR 180 Project

##__IMPORTANT NOTES:__

everything in /public/ is publicly viewable.

other stuff may be publicly viewable.

#Server Requirements

This project runs on Ubuntu 64bit server, with Ruby, Python, MySQL, and a number of plug-ins.

Port 80 must be open.

IP address "bind" must be changed in the _server.rb_ source file.

#Project layout

##/public/

Contains all files that the website will load

##/data_import/

Contains scripts to load data into the database. Currently, it doesn't read the config file, thus __the source code must contain the database username / password__, and doesn't support non-MySQL databases.

##/config/

Contains _database.conf_, which tells _server.rb_ where the database is.

##/test-file/

Contains test .csv files which can be loaded into the database.

#Credits 

CSS is modified from https://github.com/revolunet/sublimetext-markdown-preview


