DROP TABLE IF EXISTS `experiences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `experiences` (
  `exp_id` tinytext,
  `exp_name` text,
  `exp_description` text,
  `start_lat` float DEFAULT NULL,
  `start_lon` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `steps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `steps` (
  `step_name` text,
  `step_description` text,
  `step_id` tinytext,
  `exp_id` tinytext,
  `lat` float DEFAULT NULL,
  `lon` float DEFAULT NULL,
  `required_level` int(11) DEFAULT NULL,
  `output_level` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `user_spots`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_spots` (
  `exp_id` tinytext,
  `step_id` tinytext,
  `user_id` tinytext,
  `lat` float DEFAULT NULL,
  `lon` float DEFAULT NULL,
  `time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
