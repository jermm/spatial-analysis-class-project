// info_page.js
function parseURL()
{
	pageURL = location.pathname;
	var urlParts = pageURL.split("/");

	var exper = "";
	var experFound = 0;
	var mission = "";
	var missionFound = 0;

	// exper name
	if (urlParts[2] != undefined && urlParts[2] != "")
	{
		exper = urlParts[2];
		experFound = 1;
	}

	//mission name
	if (urlParts[3] != undefined && urlParts[3] != "")
	{
		mission = urlParts[3];
		missionFound = 1;
	}

	var urlInfo = {};

	urlInfo["exp_name"] = exper;
	urlInfo["step_name"] = mission;

	return urlInfo;
}

function loadList()
{
	console.log("loading list.");
	$("#mid").html("Waiting for server...");

	document.title = "User Info | Experiences";

	$("#top").html("<h2>" + document.title + ":</h2>");

	var call = "http://locusgame.net/api/type=list_all_exps";

	$.get(call, function( data)
	{
		console.log(data);

		var output = "";

		for (var key in data)
		{
			var expURL = "/info/" + data[key]["exp_name"];

			output += "<a href=" + expURL + ">";
			output += "<b>" + data[key]["exp_name"].replace(/_/g, ' ') + "</b></a>:<br>"
			output += data[key]["exp_description"];
			output += "<p></p>";
		}

		$("#mid").html(output);

	});
}

function loadSteps(urlInfo)
{
	console.log("loading steps.");
	console.log(urlInfo);

	document.title = "User Info | " + urlInfo["exp_name"].replace(/_/g, ' ');
	$("#top").html("<h2>" + document.title + ":</h2>");

	var call = "http://locusgame.net/api/type=list_all_steps/" + urlInfo["exp_name"];

	$.get(call, function( data )
	{
		$("#mid").empty(); //clear loading (probably a better way to do this...)

		console.log(data);

		for (var key in data)
		{
			var output = "";
			var expURL = "/info/" + urlInfo["exp_name"] + "/" + data[key]["step_name"];

			output += "<div id= \"" + data[key]["step_name"] + "\" ></div>";
			$("#mid").append(output);

			output = "<a href=" + expURL + ">";
			output += "<b>" + data[key]["step_name"].replace(/_/g, ' ') + "</b></a>"

			$("#" + data[key]["step_name"]).html(output);
		}
	});
}

var map = ""; //the google map goes here.

function initialize(mapOptions) {
	console.log("loading maps");

	map = new google.maps.Map(document.getElementById('map-canvas'),
	mapOptions);

}

function loadMaps(data)
{
	console.log("waiting for maps");
	console.log(data);

	// set mapoptions
	var mapOptions = {
		center: { lat: data["actual_lat"], lng: data["actual_lon"]},
		zoom: 13
		};

	// generate map object
	google.maps.event.addDomListener(window, 'load', initialize(mapOptions));

	$("#bottom").html("<br>");

	// add all the points to the map
	var points = {};
	var markers = {};

	points["org"] = new google.maps.LatLng(data["actual_lat"], data["actual_lon"]);

	var marker = new google.maps.Marker({
		position: points["org"],
		map: map,
		title: "Actual Point."
	});

	// add ALL THE POINTS.
	for (var key in data)
	{
		// there def. is a better way to do this...
		if (key == "actual_lat" || key == "actual_lon")
		{
			continue;
		}		
		points[key] = new google.maps.LatLng(data[key]["lat"], data[key]["lon"]);

		var title = "User point " + key + "\n" + data[key]["distance"].toFixed(2) + "km away.";

		markers[key] = new google.maps.Marker({
			position: points[key],
			map: map,
			title: title
		});
	}
}

function loadCheckIns(urlInfo)
{

	console.log("loading checkins.");
	console.log(urlInfo);

	var call = "http://locusgame.net/api/type=user_stats/exp_name=" + urlInfo["exp_name"] + "/step_name=" + urlInfo["step_name"];
	console.log(call);

	document.title = urlInfo["exp_name"].replace(/_/g, ' ') + " | " + urlInfo["step_name"].replace(/_/g, ' ');
	$("#top").html("<h2>" + document.title+ ":</h2>");

	// put in the google map:
	$("#bottom").html("Maps are loading...");

	// load data into the page.
	$.get(call, function( data )
	{
		// clear "loading"
		$("#mid").empty();

		// create the info table inside mid.
		var output = "<table id=\"info\" class=\"display\" cellspacing=\"0\" width=\"100%\">";
		output += "<thead><tr><td>User ID</td><td>Location</td><td>Distance from target (km)</td><td>Time</td></tr></thead><tbody>";

		$("#mid").html(output);

		// add data into "info" table.
		for (var key in data)
		{
			// there def. is a better way to do this...
			if (key == "actual_lat" || key == "actual_lon")
			{
				continue;
			}

			var output = "<tr><td>" + data[key]["user_id"] + "</td><td>";
			output += data[key]["lat"].toFixed(4) + ", " + data[key]["lon"].toFixed(4) + "</td><td>";
			output += data[key]["distance"].toFixed(2) + "</td><td>";
			output += data[key]["time"] + "</td></tr>"
			$("#info").append(output);
		}

		// close the table
		$("#info").append("</tbody></table>");
		console.log($("#mid").html());		
		$("#info").DataTable();

		// load maps.
		loadMaps(data);
	});
}

// load the page based on the url.
function generatePage()
{

	var urlInfo = parseURL();
	console.log(urlInfo);

	if (urlInfo["exp_name"] == "")
	{
		loadList();
		return;
	}

	if (urlInfo["step_name"] == "")
	{
		loadSteps(urlInfo);
		return;
	}

	loadCheckIns(urlInfo);
}

generatePage();