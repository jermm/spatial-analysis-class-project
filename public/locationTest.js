// test program.

<style type="text/css">
      html, body, #map-canvas { height: 90%; }
    </style>

 <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script>
var x = $("#demo");

x.html("Waiting for location...");

function getLocation() {
    if (navigator.geolocation) {
    	x.html("Getting Location...");
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        x.html("Geolocation is not supported by this browser.");
    }
}
function showPosition(position) {
    x.html("Latitude: " + position.coords.latitude +
    "<br>Longitude: " + position.coords.longitude);

    addLocation(position);

    // change page title.
    document.title = "Your Location Is:";

    // change URL -> I don't really know how this works.
    history.pushState("1","1","/location/testing");
}
function urlTest()
{

  var pageURL = location.pathname;
  var urlParts = pageURL.split("/");
  console.log(urlParts);


}

</script>

<script type="text/javascript"
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDgPX5uF9On_hab-tiKROlCt-JkK6dHyG4">
</script>

<script type="text/javascript">

      var map;

      function initialize() {
        var mapOptions = {
          center: { lat: -34.397, lng: 150.644},
          zoom: 8
        };

        map = new google.maps.Map(document.getElementById('map-canvas'),
            mapOptions);

        }

        function addLocation(position)
        {
          var userPoint = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
          var marker = new google.maps.Marker({
            position: userPoint,
            map: map,
            title:"you are here."
          });

          map.panTo(userPoint);

          var title = $("h2");

          title.html("You are here:")

        }

        
      google.maps.event.addDomListener(window, 'load', initialize);
    </script>
