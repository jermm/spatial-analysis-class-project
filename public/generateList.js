// generateList.js
// the homepage.

function getLocation()
{
	if(navigator.geolocation)
	{
		$("#top-status").html("<h2>Getting Location...</h2>");
		document.title = "Waiting for location";
		navigator.geolocation.getCurrentPosition(getList);	
	}

	else
	{
		$("h2").html("Issue getting location.");
		$("#list").html("Unnamed School Project requires fine location.");
	}
}

function getList(position)
{
	document.title = "Untited school project";

	$("#top-status").html("<h2>Experiences near you:</h2>");
	$("#status").html("Found Location.");
	$("#list").html("Getting list...");	
	$("#location").html("lat: " + position.coords.latitude.toFixed(4) + "<br>" + "lon: " + position.coords.longitude.toFixed(4));

	var call = "http://locusgame.net/api/type=near_experiences/lat=" + position.coords.latitude + "/lon=" + position.coords.longitude;
	console.log(call);

	$.get(call, function( data )
	{
		$("#list").html("generating list...");			

		console.log(data);

		var output = "";

		for (var key in data["experiences"])
		{
			var expNameSafe = data["experiences"][key]["name"].replace(/_/g, ' ');
			var expURL = data["experiences"][key]["name"];

			console.log(data["experiences"][key]["name"]);
			output += "<a href=" + expURL + ">"
			output += "<b>" + expNameSafe + "</b></a>" + ": " + data["experiences"][key]["description"];
			output += "<br>";
			output += "Distance from you: " + data["experiences"][key]["distance"].toFixed(3) + "km";
			output += "<p></p>";

		}

		$("#list").html(output);
		$("#location").prepend("<h2>Location:</h2>");
	});
}

// run page.
getLocation();
