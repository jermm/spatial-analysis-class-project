// game.js
var missionData = "";
var nextMissionData = "";
var exper = "";
var experTitleSafe = "";
var missionFromURL = "";
var id = Math.floor(Math.random() * 1000000);

function getLocation()
{
	var options = {
		enableHighAccuracy: true,
		maximumAge: 10000
	}

	if(navigator.geolocation)
	{
		$("#top").html("<h2>Getting Location...</h2>");
		$("#status").html("Waiting for location.");
		navigator.geolocation.getCurrentPosition(checkMission, error, options);	
	}

	else
	{
		$("#top").html("<h2>Issue getting location.</h2>");
		$("#status").html("Unnamed School Project requires fine location.");
	}
}

function error()
{
	console.log("something isn't working");
	$("#top").html("<h2>Issue getting location</h2>");
	$("#status").html("You may need to reload the page.");
}

function getMissionInformation()
{
	var pageURL = location.pathname;
	var urlParts = pageURL.split("/");
	exper = "";
	var mission = ""; //TODO -> make it clear that expr and mission are drawn from the URL and may not exist.

	var experFound = 0;
	var missionFound = 0;

	console.log(urlParts);
	console.log("Length: " + urlParts.length);

	// exper name
	if (urlParts[1] != undefined && urlParts[1] != "")
	{
		exper = urlParts[1];
		experTitleSafe = exper.replace(/_/g, ' ');
		experFound = 1;
	}

	//mission name
	if (urlParts[2] != undefined && urlParts[2] != "")
	{
		mission = urlParts[2];
		missionFromURL = mission;
		missionFound = 1;
	}

	console.log("exper: " + exper + "\n" + "mission: " + mission);
	
	var call = "";

	// need to find out the first mission.
	if (experFound == 1 && missionFound == 0)
	{
		call = "http://locusgame.net/api/type=get_step/exp_name=" + exper + "/step_name=\"\""
		console.log(call);
	}

	// "resuming" an experence at this location.
	if (experFound == 1 & missionFound == 1)
	{
		call = "http://locusgame.net/api/type=get_step/exp_name=" + exper + "/step_name=" + mission
		console.log(call);
	}

	$.get(call, function(data)
	{
		// console.log(data);
		missionData = data;

		drawMission();
	});
}

function drawMission()
{
	console.log(missionData);

	// check if the mission was found
	// if the mission wasn't found -> do something else.
	if (missionData["title"] == 0)
	{
		console.log("experence not found.");

		$("#top").html("<h2>" + "Not found." + "</h2>");
		$("#mid").html("Couldn't find " + exper + ". It might not exist.");

		return;
	}

	if (missionData["step"] == 0)
	{
		console.log("Mission not found.");

		$("#top").html("<h2>" + "Mission not found." + "</h2>");
		$("#mid").html("Couldn't find " + missionFromURL + ". It might not exist.");

		return;
	}

	var missionTitleSafe = missionData["step_name"];
	missionTitleSafe = missionTitleSafe.replace(/_/g, ' ')

	var missionDisc =  missionData["step_description"];

	$("#mid").html(missionDisc);
	$("#top").html("<h2>" + experTitleSafe + " | " + missionTitleSafe + "</h2>")

	// change URL:
	var newURL = "/" + exper + "/" + missionData["step_name"];
	history.pushState("1","1",newURL);

	// change page title:
	document.title = experTitleSafe + " | " + missionTitleSafe;
}

function checkMission(position)
{
	d = new Date();
	console.log(position.coords.latitude.toFixed(3) + ", " + position.coords.longitude.toFixed(3));
	$("#top").html("<h2>" + exper + "</h2>");

	var statusMessage = "Location found at " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds() + ".<br>";
	statusMessage += "Accuracy is ";

	if (position.coords.accuracy < 50)
	{
		statusMessage += "good."
	}

	else if (position.coords.accuracy <= 150)
	{
		statusMessage += "OK."
	}
	else if (position.coords.accuracy > 150)
	{
		statusMessage += "poor, you may need to refresh the page or check location settings."
	}

	$("#status").html(statusMessage);

	console.log(Date());

	// make API call to get mission data.
	var call = "http://locusgame.net/api/type=check_step/exp_name=" +  exper + "/level=" + missionData["current_level"] + "/lat=" + position.coords.latitude + "/lon=" + position.coords.longitude;
	
	//make API "check-in" call.
	var call2 = "http://locusgame.net/api/type=check_in/id=" + id + "/exp_name=" + exper + "/step_name=" + missionData["step_name"] + "/lat=" + position.coords.latitude + "/lon=" + position.coords.longitude;
	
	console.log(call);
	console.log(call2);

	// run the mission data call.
	$.get(call, function(data)
	{
		if (data["new_step"] == 1)
		{
			missionData = data;
		}
		else
		{
			missionData["distance"] = data["distance"];
			missionData["new_step"] = 0;
		}

		// display that mission data.
		drawMission();
	});

	// run the check-in call.
	$.get(call2, function(data)
	{
		if (data["status"] == 1)
		{
			console.log("check-in OK.")
		}
	});
}

// primary loop
getMissionInformation();
