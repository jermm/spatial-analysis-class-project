import csv
import sys
import MySQLdb as mdb

if len(sys.argv) != 2:
   print "Please specify 1 Steps .CSV file."
   sys.exit()

database = "game"
password = "password_here"

mydb = mdb.connect(passwd = password, db = database)
cursor = mydb.cursor()

data = csv.reader(file(sys.argv[1]))
for row in data:
   cursor.execute("INSERT INTO steps(step_name, step_description, step_id, exp_id, lat, lon, required_level, output_level) VALUES(%s,%s,%s,%s,%s,%s,%s,%s)", row)


mydb.commit()
cursor.close()
print "Done"
