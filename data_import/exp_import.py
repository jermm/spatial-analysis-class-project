import csv
import sys
import MySQLdb as mdb

if len(sys.argv) != 2:
   print "Please specify 1 Experiences .CSV file."
   sys.exit()

database = "game"
password = "password_here"

mydb = mdb.connect(passwd = password, db = database)
cursor = mydb.cursor()

data = csv.reader(file(sys.argv[1]))
for row in data:
   cursor.execute("INSERT INTO experiences(exp_id, exp_name, exp_description, start_lat, start_lon) VALUES(%s,%s,%s,%s,%s)", row)


mydb.commit()
cursor.close()
print "Done"
